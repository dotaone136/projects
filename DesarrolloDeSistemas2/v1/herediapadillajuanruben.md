# proyecto formativo: Modelado Backend de un sistema de Una Farmacia
- Juan Ruben Heredia Padilla
- [Facebook](https://www.facebook.com/juanruben.herediapadilla.5/)

##	Introducción
este sistema esta creado para la gestion de eventos de una Farmacia usando Laragon como el medio para poder realizar el desarrollo junto a visual studio code como editor de los codigos, en este proyecto se trabajo con tablas, modelos, seeders y Api.

Este desarrollo de un sistema se centra en lo que un sistema para una farmacia pueda requerir por ejemplo puede tener los datos de los productos que tendra la farmacia y tambien un control sobre su venta y controles que algunos farmacos necesitan con ser la receta, tambien puede proporcionar un detallada informacion sobre el cliente y lo que llevara tambien el uso de usurios.

##	Objetivos
El objetivo de este proyecto es desarrollo un sistema de gestion de eventos sobre una Farmacia Padilla en la ciudad de Santa Cruz de la Sierra tambien tiene que tener:
1. Analisis y Modelado de las clases que ocupara el sistema entre ellos que el sistema pueda tener datos de clientes, productos, proveedores, recetas y empleados, se puede considerar aumentar otras datos si se cree conveniente.
2. Migraciones y Modelos con los que se podra obtener los datos y asociaciones de todas las tablas que ocupara la Farmacia
3. Seeder para poder tener datos de prueba y poder comprobar el funcionamiento de los modelos
4. Api con el que podremos gestionar los datos que ingresemos y tambien su modificacion o eliminacion de los mismos. 

##	Marco Teórico
###	Laravel
Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```
3. **Creacion de una base de datos en laravel**: Vamos al menu de laragon en MySQL y en HeidiSQL , creamos una nueva base de datos para el proyecto

4. **Migracion**: Para la migracion de los datos a la Base de datos creada ocupamos:

```bash
php artisan key:generate
php artisan migrate
```

5. **Modelos**: Utilizamos el siguiente comando para la creacion de los nuevos modelos que ocuparemos para las tablas creadas en tu proyecto
```bash
php artisan make:model nombre-del-modelo
```

6. **Seeder**: Para la creacion de los seeders, para poder probar que todo este bien en el proyecto:

```bash
php artisan make:seeder nombre-del-seeder
```
7. **Levantar el serve**: para poder ver el serve en laragon ocupamos el siguiente codigo:

```bash
php artisan serve
```

###	MVC
El MVC (Modelo-Vista-Controlador) es un software que nos ayuda a separar la logica que ocupara un negocio o empresa y esta son:
1. **Modelo**: Esto es la logica de nuestro negocio y los datos que ocuparemos en laravel, los modelos lo ocupamos para interactuar con las tablas que ocupara el negocio
2. **Vista**: Es la forma en como vemos la informacion en la interfaz de usuario
3. **Controlador**: el controlador es donde podemos implementar la logica de la aplicacion, esto actuara como una unterfaz entre los componentes del modelo y la vista

##	Metodología
Para la creacion del proyecto:
1. primero ocupe un diagrama de clase en StarUML para poder tener una vision mas amplia de las tablas que ocupare en mi proyecto 
2. despues ocupamos laragon para poder empezar el proyecto
##	Modelado o Sistematización
### Diagrama de clases
![diagrama](https://live.staticflickr.com/65535/53558968320_afdcc78b35_b.jpg)

### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        //tabla reseña
        Schema::create('reseña', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion')->nullable();                        
            $table->string('calificacion')->nullable(); 
            
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla empleado
        Schema::create('empleado', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('cargo')->nullable();
            $table->string('email')->nullable(); 
            $table->string('telefono')->nullable(); 

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
            $table->softDeletes();
        });

         //tabla cliente
        Schema::create('cliente', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('direccion')->nullable();
            $table->string('telefono')->nullable(); 
            
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla receta
        Schema::create('receta', function (Blueprint $table) {
            $table->id();
            $table->string('fecha_emision')->nullable();                        
            $table->string('paciente')->nullable();
            $table->string('medicamento')->nullable(); 
            $table->string('medico')->nullable(); 

            $table->unsignedBigInteger('cliente_id');
            $table->foreign('cliente_id')->references('id')->on('cliente');
            
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla venta
        Schema::create('venta', function (Blueprint $table) {
            $table->id();
            $table->string('fecha_hora')->nullable();                        
            $table->string('total')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla inventario
        Schema::create('inventario', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('stock')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla categoria
        Schema::create('categoria', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('descripcion')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla proveedor
        Schema::create('proveedor', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('direccion')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla producto
        Schema::create('producto', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();            
            $table->string('descripcion')->nullable();            
            $table->string('precio')->nullable();  
            $table->string('fecha_caducidad')->nullable();            
            $table->string('stock')->nullable();            

            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categoria');

            $table->unsignedBigInteger('inventario_id');
            $table->foreign('inventario_id')->references('id')->on('inventario');

            $table->unsignedBigInteger('proveedor_id');
            $table->foreign('proveedor_id')->references('id')->on('proveedor');

            $table->timestamps();
            $table->softDeletes();            
        });

        // tabla muchos a muchos detalle_venta
        Schema::create('detalle_venta', function (Blueprint $table) { 

            $table->unsignedBigInteger('producto_id');
            $table->foreign('producto_id')->references('id')->on('producto');

            $table->unsignedBigInteger('venta_id');
            $table->foreign('venta_id')->references('id')->on('venta');

            $table->primary(['producto_id','venta_id']);

            $table->timestamps();
            $table->softDeletes();
        });
        






        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('detalle_venta');
        Schema::dropIfExists('producto');
        Schema::dropIfExists('proveedor');
        Schema::dropIfExists('categoria');
        Schema::dropIfExists('inventario');
        Schema::dropIfExists('venta');
        Schema::dropIfExists('receta');
        Schema::dropIfExists('cliente');
        Schema::dropIfExists('empleado');
        Schema::dropIfExists('reseña');
    }
}


```
### explicacion del un caso de modelado.
Para la creacion de las funciones del modelo hacemos lo siguiente:
1. Creamos el modelo en la terminal de laragon.
```bash
php artisan make:model Receta
php artisan make:model Cliente
```
2. Ahora revisamos que los modelos se hayan creado correctamente una vez que comprobamos que 
los modelos estan creados pasamos a añadir lo que seria lo siguiente que seria el
use de softDeletes, tambien añadimos dentro de la clase Receta el nombre de la tabla a la cual esta unida y ponemos los atributos de la tabla.
3. Por ultimos creamos la funcion con la cual tendremos que hacer una conexion dependiendo si es uno a muchos, o muchos a muchos.
Tendria que quedar de la siguiente manera:
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Receta extends Model
{
    protected $fillable=['fecha_emision','paciente','medicamento','medico']; 
    protected $table = 'receta';
    use HasFactory;
    use SoftDeletes;

    public function Clientes() {
        return $this->belongsTo(Cliente::class);
    }
}

```
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Cliente extends Model
{
    protected $fillable=['nombre','direccion','telefono']; 
    protected $table = 'cliente';
    use HasFactory;
    use SoftDeletes;

    public function Ventas(){
        return $this->hasMany(Venta::class);
    }

    public function Recetas(){
        return $this->hasMany(Receta::class);
    }
}

```
            
### Datos seeder
Los seeders son archivos en PHP que vamos a utilizar en nuestro proyecto para crear datos de prueba y comprobar la funcionalidad de nuestro sistema de gestion para nuestra Farmacia.
Ahora lo primero que necesitamos hacer es lo siguiente:
1. entramos a la terminal de laragon y creamos los seeder, podemos crear seeders para cada tabla o podemos hacer una tabla general donde creamos todos los datos:
```bash
php artisan make:seeder GeneralSeeder
```
2. Ahora primero tenemos que poner la direccion de los modelos que ocupara nuestros seeders para poder insertar los datos una vez hacemos eso creamos los seeder de la siguiente manera:
```php
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Reseña;
use App\Models\Empleado;
use App\Models\Receta;
use App\Models\Cliente;
use App\Models\Proveedor;
use App\Models\Inventario;
use App\Models\Categoria;
use App\Models\Producto;
use App\Models\Venta;



class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Reseña::create(['descripcion' => 'Excelente servicio y atención al cliente.', 'calificacion' => 5, 'user_id' => 1]);
        Reseña::create(['descripcion' => 'Gran variedad de productos y precios accesibles.', 'calificacion' => 4, 'user_id' => 2]);
        Reseña::create(['descripcion' => 'Entrega rápida y eficiente.', 'calificacion' => 4, 'user_id' => 3]);
        Reseña::create(['descripcion' => 'Buena experiencia de compra, pero podrían mejorar la atención al cliente.', 'calificacion' => 3, 'user_id' => 4]);
        Reseña::create(['descripcion' => 'Productos de calidad, pero el proceso de compra fue un poco confuso.', 'calificacion' => 3, 'user_id' => 5]);
        Reseña::create(['descripcion' => 'El pedido llegó tarde y algunos productos estaban dañados.', 'calificacion' => 2, 'user_id' => 6]);
        Reseña::create(['descripcion' => 'Mal servicio al cliente, no responden a las consultas.', 'calificacion' => 1, 'user_id' => 7]);
        Reseña::create(['descripcion' => 'Los precios son altos y la calidad de los productos es mediocre.', 'calificacion' => 2, 'user_id' => 8]);
        Reseña::create(['descripcion' => 'La página web es difícil de navegar y el proceso de compra es lento.', 'calificacion' => 2, 'user_id' => 9]);
        Reseña::create(['descripcion' => 'No recomendaría esta farmacia, la experiencia de compra fue muy mala.', 'calificacion' => 1, 'user_id' => 10]);

        Empleado::create(['nombre' => 'Empleado 1', 'cargo' => 'Cajero', 'email' => 'empleado1@gmail.com', 'telefono' => '6958574', 'user_id' => 11]);
        Empleado::create(['nombre' => 'Empleado 2', 'cargo' => 'Cajero', 'email' => 'empleado2@gmail.com', 'telefono' => '6955574', 'user_id' => 12]);
        Empleado::create(['nombre' => 'Empleado 3', 'cargo' => 'Cajero', 'email' => 'empleado3@gmail.com', 'telefono' => '6966674', 'user_id' => 13]);
        Empleado::create(['nombre' => 'Empleado 4', 'cargo' => 'Farmacéutico', 'email' => 'empleado4@gmail.com', 'telefono' => '6956474', 'user_id' => 14]);
        Empleado::create(['nombre' => 'Empleado 5', 'cargo' => 'Farmacéutico', 'email' => 'empleado5@gmail.com', 'telefono' => '6986574', 'user_id' => 15]);

        Cliente::create(['nombre' => 'Jorge Pérez', 'direccion' => 'Av. Alemana #123', 'telefono' => '123456']);
        Cliente::create(['nombre' => 'Juan López', 'direccion' => 'Av. Monserrat #456', 'telefono' => '987654']);
        Cliente::create(['nombre' => 'María Sánchez', 'direccion' => 'Av. Banzer #789', 'telefono' => '456123']);
        Cliente::create(['nombre' => 'Carlos García', 'direccion' => 'Av. Cristo Redentor #321', 'telefono' => '789456']);
        Cliente::create(['nombre' => 'Ana Rodríguez', 'direccion' => 'Av. Alemana #987', 'telefono' => '654321']);

        Receta::create(['fecha_emision' => '2024-01-11', 'paciente' => 'Juan Pérez', 'medicamento' => 'Jarabe 1', 'medico' => 'Dr. López', 'cliente_id' => 1]);
        Receta::create(['fecha_emision' => '2024-01-12', 'paciente' => 'María Sánchez', 'medicamento' => 'Jarabe 2', 'medico' => 'Dr. Rodríguez', 'cliente_id' => 2]);
        Receta::create(['fecha_emision' => '2024-01-18', 'paciente' => 'Carlos García', 'medicamento' => 'Jarabe 3', 'medico' => 'Dr. Gómez', 'cliente_id' => 3]);


        // Creación de inventarios
        Inventario::create(['nombre' => 'jarabe', 'stock' => 100]);
        Inventario::create(['nombre' => 'pastilla', 'stock' => 100]);
        Inventario::create(['nombre' => 'compresa', 'stock' => 100]);

        // Creación de categorías
        Categoria::create(['nombre' => 'jarabe b', 'descripcion' => 'Jarabe para la tos']);
        Categoria::create(['nombre' => 'jarabe a', 'descripcion' => 'Jarabe para la fiebre']);
        Categoria::create(['nombre' => 'jarabe c', 'descripcion' => 'Jarabe para dormir']);

        // Creación de proveedores
        Proveedor::create(['nombre' => 'Proveedor 1', 'direccion' => 'Los Alamos', 'email' => 'proveedor1@gmail.com', 'telefono' => '111111']);
        Proveedor::create(['nombre' => 'Proveedor 2', 'direccion' => '3er anillo', 'email' => 'proveedor2@gmail.com', 'telefono' => '222222']);

        // Creación de ventas
        Venta::create(['fecha_hora' => '2024-03-03', 'total' => 25, 'user_id' => 1, 'cliente_id' => 1]);
        Venta::create(['fecha_hora' => '2024-03-03', 'total' => 100, 'user_id' => 2, 'cliente_id' => 2]);
        Venta::create(['fecha_hora' => '2024-03-03', 'total' => 250, 'user_id' => 3, 'cliente_id' => 3]);

        // Creación de productos
        Producto::create(['nombre' => 'jarabe a', 'descripcion' => 'Jarabe para la fiebre', 'precio' => 10, 'fecha_caducidad' => '2024-05-11', 'stock' => 10, 'categoria_id' => 1, 'inventario_id' => 1, 'proveedor_id' => 1]);
        Producto::create(['nombre' => 'jarabe b', 'descripcion' => 'Jarabe para la tos', 'precio' => 15, 'fecha_caducidad' => '2024-08-18', 'stock' => 20, 'categoria_id' => 2, 'inventario_id' => 2, 'proveedor_id' => 2]);
        Producto::create(['nombre' => 'jarabe c', 'descripcion' => 'Jarabe para dormir', 'precio' => 50, 'fecha_caducidad' => '2024-09-01', 'stock' => 15, 'categoria_id' => 3, 'inventario_id' => 3, 'proveedor_id' => 1]);

        // Asociación de productos a ventas
        $venta01 = Venta::find(1);
        $venta01->productos()->attach([1, 2, 3]);

        $venta02 = Venta::find(2);
        $venta02->productos()->attach([1, 2]);
    }
}


```
### Seeder de los Usuarios
```php
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Factory as Faker;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,100) as $i){
            User::create([
                'name'=>$faker->name,
                'paterno'=>$faker->lastName,
                'materno'=>$faker->lastName,
                'email'=>$faker->unique()->safeEmail,
                'password'=>bcrypt('1234567'),
            ]);
        }
        
        
    }
}

```
3. ahora tenemos que ejecutar los seeders que creamos en la terminal de laragon.
```bash
php artisan db:seed
```
### APi.
en la API vamos a acceder a los datos que hemos creado en nuestra base de datos de nuestro sistema.

- tenemos que activar el serve de laragon con la cual nos dara una direccion de ip donde tendremos que ingresar para poder ver los datos
```bash
php artisan serve
```
- ahora tenemos que crear las rutas de los datos que ocuparemos por ejemplo si queremos los datos de los clientes tenemos que poner lo siguiente:
```php
Route::get('Clientes',function(){
    return Cliente::all();
});
```
- como se puede observar aqui creamos la direccion de la pagina la cual se llama Clientes y retornara los datos del modelo Cliente, aunque tambien si queremos algo mas especifico tambien se podrias hacer.

##	Conclusiones
Al concluir el proyecto podemos ver la implementacion de los objetivos que teniamos propuesto con la farmacia, tambien vimos los modelado, seeders y migraciones que ocupara con estos pasos finalizados vimos el diseño y desarrollo de nuestro sistema eficiente y funcional para la gestio de la farmacia.
Aunque de ser necesario tambien se puede aumentar otras datos segun requiera el cliente.

##	Bibliografía
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234
- https://desarrolloweb.com/articulos/laragon.html
##	Anexos
### foto de la empresa para la cual estan desarrollando
![Logo](https://live.staticflickr.com/65535/53568779165_a801c713f2_z.jpg)
### ubicacion

![diagrama](https://live.staticflickr.com/65535/53568248421_c381c979ec_z.jpg)
