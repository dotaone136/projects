#                                 PROYECTO FORMATIVO: 
# Modelado Backend de un Sistema de Gestión para la Clínica Veterinaria "MI-MASCOTA"
- Estudiante: Mario Barba U.
- [Facebook](https://www.facebook.com/marhio.barba.1)
- Docente: Ing. Jaime Sanbrana Ch.

##	Introducción
El sistema tiene el propósito de facilitar la administración y gestión de una clínica veterinaria, proporcionando herramientas para el manejo de información relacionada con pacientes (mascotas), propietarios, citas, facturas, historiales médicos, veterinarios, vacunas y laboratorios.

##	Objetivos
Desarrollar un sistema de gestión mediante la aplicación de conocimientos adquiridos en el módulo, con el fin de realizar el Analisis, Modelado, Migraciones, Models, Seeder y la Api REst para el Sistema de Gestión de la Clínica Veterinaria Mi-Mascota de la ciudad de Santa Cruz de la Sierra.

##	Marco Teórico

###	Laravel

**Instalación del Proyecto**
Puedes utilizar el siguiente comando para crear tu proyecto
```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```
o descargar el proyecto, siguir estos pasos para poder instalarlo:

1. Mueve la carpeta del proyecto descomprimido a la carpeta de proyectos de tu servidor local. 
   Por ejemplo, si estás utilizando Laragon, puedes mover la carpeta a C:/laragon/www.
2. Abre tu terminal y por línea de comandos navega hasta la carpeta del proyecto C:/laragon/www/veterinaria
3. Ejecuta el siguiente comando desde la terminal de Laragon para instalar las dependencias del proyecto.
```bash
composer install
```
4. Copia el archivo .env.example y pégalo como .env.
5. Genera una nueva clave de aplicación con el siguiente comando desde la terminal de laragon.
```bash
php artisan key:generate
```
6. Revisa y configura la conexión a la base de datos en el archivo .env.
7. Ejecuta las migraciones de la base de datos con el siguiente comando.
```bash
php artisan migrate
```
8. Inicia el servidor local con el siguiente comando.
```bash
php artisan serve
```
9. Abre tu navegador web y navega a la URL proporcionada por el servidor local para acceder a la  aplicación.

###	MVC
El Modelo-Vista-Controlador (MVC) es un patrón de arquitectura de software que divide una aplicación en tres componentes principales para organizar y gestionar la lógica del software de manera más eficiente.

Este patrón es ampliamente utilizado en el desarrollo de software y se ha convertido en un estándar en muchos marcos y entornos de desarrollo web, como Laravel (PHP), Django (Python), Ruby on Rails (Ruby), y Angular (JavaScript), entre otros.

Aquí está una descripción de cada componente:

1. **Modelo:**
   - Representa la lógica del negocio y los datos de la aplicación.
   - Se encarga del acceso, manipulación y almacenamiento de los datos.
   - No tiene conocimiento directo de la interfaz de usuario ni de cómo se presentan los datos.

2. **Vista:**
   - Representa la interfaz de usuario y la presentación de los datos.
   - Recibe datos del Modelo y los muestra al usuario.
   - Responde a las acciones del usuario, como clics o entradas de teclado, y actualiza la interfaz según sea necesario.
   - No tiene conocimiento directo de la lógica del negocio.

3. **Controlador:**
   - Actúa como intermediario entre el Modelo y la Vista.
   - Recibe las interacciones del usuario y actualiza el Modelo o la Vista según sea necesario.
   - Contiene la lógica de la aplicación y coordina las acciones del usuario y las actualizaciones de datos.
   - No tiene conocimiento directo de cómo se almacenan ni se presentan los datos.

El propósito del patrón MVC es proporcionar una separación clara de responsabilidades entre los diferentes componentes, lo que facilita el desarrollo, la prueba y el mantenimiento del software. Al dividir la aplicación en estos tres componentes, los cambios en uno de ellos (por ejemplo, la lógica del negocio) no afectan directamente a los demás (interfaz de usuario, almacenamiento de datos), lo que mejora la modularidad y la escalabilidad del código.

##	Metodología

La elección de la metodología depende de varios factores, incluyendo el tipo y tamaño del proyecto, la naturaleza de los requisitos y las preferencias del equipo de desarrollo. Mucho adaptan o combinan metodologías según sus necesidades específicas. En nuestro caso preferimos Scrum

Scrum proporciona un marco flexible que se adapta a las cambiantes necesidades del proyecto y prioriza la entrega continua de valor al cliente.

1. Identificar los objetivos y requisitos del proyecto.
Crear un Product Backlog que enumere todas las funcionalidades deseadas, priorizadas por su valor para el cliente.
Seleccionar el Equipo Scrum, que incluye al Product Owner, Scrum Master y el Equipo de Desarrollo.
Planificación de la Sprint (Iteración):

2. Realizar una reunión de Planificación de la Sprint para seleccionar elementos del Product Backlog para incluir en la Sprint Backlog.
Estimar el esfuerzo necesario para completar cada elemento de la Sprint Backlog.
Desarrollo de la Sprint:

3. El equipo trabaja en el desarrollo de las funcionalidades seleccionadas durante la Sprint.
Las reuniones diarias de Scrum (Daily Scrum) se llevan a cabo para mantener al equipo informado sobre el progreso y abordar cualquier obstáculo.
Revisión de la Sprint:

4. Al final de la Sprint, se lleva a cabo una reunión de Revisión de la Sprint para demostrar las funcionalidades completadas al Product Owner y a otras partes interesadas.
Se recopila el feedback y se ajusta el Product Backlog según sea necesario.
Retrospectiva de la Sprint:

5. El equipo realiza una retrospectiva para analizar qué salió bien, qué se podría mejorar y cómo ajustar el proceso para la siguiente Sprint.
Se identifican acciones para mejorar la eficiencia y la calidad del trabajo del equipo.
Actualización del Product Backlog:

6. El Product Owner actualiza el Product Backlog según el feedback del cliente y las prioridades cambiantes.
Se revisa y ajusta la planificación del proyecto según sea necesario.
Iteraciones Sucesivas:

7. Los pasos del 2 al 6 se repiten en iteraciones sucesivas (Sprints) hasta que se complete el proyecto.
Cada Sprint entrega un incremento de producto potencialmente entregable.
Cierre del Proyecto:

8. Una vez que se han completado todas las Sprints y se han cumplido los objetivos del proyecto, se lleva a cabo una revisión final y se entrega el producto al cliente.
Mejora Continua:

9. Después de cada Sprint, se aplican lecciones aprendidas para mejorar los procesos y la eficiencia en las siguientes iteraciones.
Scrum Master juega un papel clave en facilitar la mejora continua y eliminar obstáculos.
Colaboración y Transparencia:

10. La colaboración constante entre el equipo, el Product Owner y otras partes interesadas es esencial.
La transparencia se mantiene a través de tableros Kanban, reuniones regulares y comunicación abierta.


### Planificación del Proyecto:

#### Día 1: Lunes

**Mañana:**
1. **Sesión de Planificación del Sprint (2 horas):**
   - Seleccionar y priorizar funcionalidades clave del Product Backlog.
   - Crear la Sprint Backlog con tareas estimadas.
  
**Tarde:**
2. **Inicio del Desarrollo:**
   - Comienzo de las tareas identificadas en la Sprint Backlog.

#### Día 2: Martes

**Durante el Día:**
3. **Desarrollo Continuo:**
   - Trabajo en las tareas de la Sprint Backlog.
   - Reuniones diarias breves de Scrum para sincronización.

#### Día 3: Miércoles

**Durante el Día:**
4. **Desarrollo Continuo:**
   - Enfoque en la implementación de funcionalidades.
   - Reuniones diarias de Scrum.

#### Día 4: Jueves

**Mañana:**
5. **Terminar Desarrollo (2 horas):**
   - Concluir la implementación de funcionalidades.
   - Preparar demostración para la Revisión de la Sprint.

**Tarde:**
6. **Revisión de la Sprint (1 hora):**
   - Demostrar funcionalidades completadas al Product Owner.
   - Recopilar feedback.

#### Día 5: Viernes

**Mañana:**
7. **Retrospectiva de la Sprint (1 hora):**
   - Evaluar el rendimiento del equipo.
   - Identificar mejoras y lecciones aprendidas.

**Tarde:**
8. **Ajuste del Product Backlog y Planificación para la Próxima Semana (2 horas):**
   - Reunión con el Product Owner para ajustar prioridades.
   - Planificación de la siguiente Sprint.

#### Día 6: Sábado

**Durante el Día:**
9. **Desarrollo Adicional y Ajustes (opcional):**
   - Realizar tareas pendientes o ajustes según el feedback.
   - Reuniones cortas si es necesario.

#### Día 7: Domingo

**Durante el Día:**
10. **Finalización y Preparación para la Entrega (opcional):**
    - Últimos ajustes y pruebas.
    - Documentación final y preparación para la entrega.

Este cronograma condensa las prácticas Scrum en una semana, siendo adaptado a la duración limitada del proyecto. La flexibilidad y adaptabilidad son clave para manejar imprevistos y cambios en los requisitos durante la semana de desarrollo. Es importante recordar que en proyectos más extensos, el ciclo de desarrollo de Scrum se repetiría en iteraciones sucesivas hasta la finalización del proyecto.

##	Modelado o Sistematización

### Diagrama de Clases

![diagrama](https://live.staticflickr.com/65535/53565813775_7c7a6e6896_z.jpg)

### Diagrama Entidad Relación.
![diagrama](https://live.staticflickr.com/65535/53565752975_59b74b5d8a_z.jpg)


### Datos Migración
```bash

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // Tabla Propietario
         Schema::create('propietarios', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('telefono');
            $table->string('direccion');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        // Tabla Mascota
        Schema::create('mascotas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('especie');
            $table->string('raza');
            $table->string('edad');
            $table->unsignedBigInteger('propietario_id');
            $table->foreign('propietario_id')->references('id')->on('propietarios');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

         // Tabla Veterinario
         Schema::create('veterinarios', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('telefono');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Tabla Cita
        Schema::create('citas', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->time('hora');
            $table->string('motivo');
            $table->unsignedBigInteger('veterinario_id');
            $table->unsignedBigInteger('mascota_id');
            $table->foreign('veterinario_id')->references('id')->on('veterinarios');
            $table->foreign('mascota_id')->references('id')->on('mascotas');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Tabla Factura
        Schema::create('facturas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cita_id');
            $table->string('nombre');
            $table->string('nit');
            $table->date('fecha');
            $table->text('descripcion');
            $table->decimal('total', 10, 2);
            $table->foreign('cita_id')->references('id')->on('citas');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

       // Tabla Vacuna
       Schema::create('vacunas', function (Blueprint $table) {
        $table->id();
        $table->string('nombre');
        $table->text('descripcion');
        $table->string('dosis');
        $table->date('fecha');
        $table->timestamp('last_used_at')->nullable();
        $table->timestamps();
        $table->softDeletes();
    });

    // Tabla Laboratorio
    Schema::create('laboratorios', function (Blueprint $table) {
        $table->id();
        $table->string('nombre');
        $table->text('descripcion');
        $table->date('fecha');
        $table->timestamp('last_used_at')->nullable();
        $table->timestamps();
        $table->softDeletes();
    });

        // Tabla Historial
        Schema::create('historiales', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mascota_id');
            $table->date('fecha');
            $table->text('diagnostico');
            $table->text('tratamiento');
            $table->unsignedBigInteger('vacuna_id')->nullable();
            $table->unsignedBigInteger('laboratorio_id')->nullable();
            $table->foreign('mascota_id')->references('id')->on('mascotas');
            $table->foreign('vacuna_id')->references('id')->on('vacunas');
            $table->foreign('laboratorio_id')->references('id')->on('laboratorios');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historiales');
        Schema::dropIfExists('laboratorios');
        Schema::dropIfExists('vacunas');
        Schema::dropIfExists('facturas');
        Schema::dropIfExists('citas');
        Schema::dropIfExists('veterinarios');
        Schema::dropIfExists('users');
        Schema::dropIfExists('mascotas');
        Schema::dropIfExists('propietarios');
    }
}

```
Comando
```bash
php artisan migrate
php artisan migrate:refresh  (ante una actualización de las tablas)
```

### Explicación del Modelado.

Este modelo refleja las relaciones entre las entidades principales de un sistema de veterinaria, considerando aspectos como citas, historiales médicos, propietarios, mascotas, facturas, entre otros. Las relaciones están definidas en función de cómo interactúan estas entidades en el contexto de un sistema veterinario. 

En el proceso de modelado, se inició analizando la información esencial para la gestión y atención de clientes en una veterinaria, partiendo de la tabla "Citas", utilizada para registrar las atenciones a las mascotas de los clientes, ya sea como requerimiento de atención urgente o programada. Se avanzó con la creación de las tablas "Veterinario", "Propietario", "Mascota", y se continuaron desarrollando las demás entidades necesarias, permitiendo estructurar progresivamente las relaciones y atributos fundamentales para el sistema veterinario.

### Datos Seeder
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Mascota;
use App\Models\Vacuna;
use App\Models\Laboratorio;
use App\Models\Cita;
use App\Models\Factura;
use App\Models\Historial;

class GeneralSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Agregar primero los datos de mascota, vacuna y laboratorio

        $mascota1 = Mascota::create(['nombre' => 'Boby', 'especie' => 'Canino', 'raza' => 'Pincher', 'edad' => '1 año', 'propietario_id' => '15']);
        $mascota2 = Mascota::create(['nombre' => 'Pisuco', 'especie' => 'Roedor', 'raza' => 'Junter', 'edad' => '3 meses', 'propietario_id' => '2']);
        $mascota3 = Mascota::create(['nombre' => 'Michi', 'especie' => 'Felino', 'raza' => 'Angora', 'edad' => '3 años', 'propietario_id' => '3']);
        $mascota4 = Mascota::create(['nombre' => 'Lorenzo', 'especie' => 'Ave', 'raza' => 'Paraba', 'edad' => '8 meses', 'propietario_id' => '8']);

        $vacuna1 = Vacuna::create(['nombre' => 'Parvovirus', 'descripcion' => 'Moquillo', 'dosis' => '3ml', 'fecha' => '2024-02-29']);
        $vacuna2 = Vacuna::create(['nombre' => 'distemper', 'descripcion' => 'Anemia', 'dosis' => '2ml', 'fecha' => '2024-02-27']);
        $vacuna3 = Vacuna::create(['nombre' => 'AVC-2', 'descripcion' => 'Desparacitación', 'dosis' => '4ml', 'fecha' => '2024-02-19']);
        $vacuna4 = Vacuna::create(['nombre' => 'Nobivac', 'descripcion' => 'Antirabica', 'dosis' => '5ml', 'fecha' => '2024-02-09']);

        $laboratorio1 = Laboratorio::create(['nombre' => 'Hematología', 'descripcion' => 'recuento de los glóbulos y de las plaquetas', 'fecha' => '2024-02-29']);
        $laboratorio2 = Laboratorio::create(['nombre' => 'Perfil tiroideo', 'descripcion' => 'análisis de hipertiroidismo o hipotiroidismo', 'fecha' => '2024-02-27']);
        $laboratorio3 = Laboratorio::create(['nombre' => 'Orina', 'descripcion' => 'cistitis, enfermedades renales o hepáticas', 'fecha' => '2024-02-19']);
        $laboratorio4 = Laboratorio::create(['nombre' => 'Cultivo bacteriano', 'descripcion' => 'infección estomacal, diarrea', 'fecha' => '2024-02-09']);

        // Luego, continuar con la creación de citas, facturas, etc.

        Cita::create(['fecha' => '2024-02-29', 'hora' => '9:45', 'motivo' => 'Vacuna', 'veterinario_id' => '1', 'mascota_id' => $mascota1->id]);
        Cita::create(['fecha' => '2024-02-27', 'hora' => '10:45', 'motivo' => 'Intoxicación', 'veterinario_id' => '2', 'mascota_id' => $mascota2->id]);
        

        //Factura::create(['cita_id' => '1', 'nombre' => 'Juan Perez', 'nit' => '1235468', 'fecha' => '2024-02-09', 'descripcion' => 'Consulta', 'total' => '150']);
        //Factura::create(['cita_id' => '2', 'nombre' => 'Maria Rodriguez', 'nit' => '1235988', 'fecha' => '2024-02-05', 'descripcion' => 'Vacuna Antirrabica', 'total' => '124']);
       

        Historial::create(['mascota_id' => $mascota1->id, 'fecha' => '2024-02-29', 'diagnostico' => 'Moquillo', 'tratamiento' => 'Antibioticos y Desparacitacion', 'vacuna_id' => $vacuna1->id, 'laboratorio_id' => $laboratorio1->id]);
        Historial::create(['mascota_id' => $mascota2->id, 'fecha' => '2024-02-27', 'diagnostico' => 'Infección de orina, Cistitis', 'tratamiento' => 'antibióticos, antiinflamatorios e hidratación ', 'vacuna_id' => $vacuna2->id, 'laboratorio_id' => $laboratorio2->id]);
        
    }
}
```
```bash
use Illuminate\Database\Seeder;
use App\Models\Veterinario;
use Faker\Factory as Faker;
class VeterinarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,5) as $i){
            Veterinario::create([
                'nombre' => $faker->firstName,
                'apellido' => $faker->lastName,
                'telefono' => $faker->phoneNumber,
            ]);
        }
        
        
    }
}
```
Comando:
```bash
php artisan db:seed --class GeneralSeeder2

php artisan db:seed --class VeterinarioSeeder
```

### Datos Api
```bash
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Models\Cita;
use App\Models\Historial;
use App\Models\Mascota;
use App\Models\Factura;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('citas',function(){
    return Cita::all();
});

Route::get('mascotas',function(){
    return Mascota::all();
});

Route::get('historiales',function(){
    return Historial::all();
});
```

##	Conclusiones
El sistema busca mejorar la eficiencia y la precisión en la gestión de la clínica veterinaria, brindando un medio centralizado para acceder y actualizar la información relacionada con las mascotas, citas, facturas y registros médicos. Además, facilita la comunicación entre veterinarios, propietarios y personal administrativo, mejorando la atención y el servicio ofrecidos en la clínica.

##	Bibliografía
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234

- Laravel Relaciones, https://kinsta.com/es/blog/laravel-relaciones/

- Laravel Eloquent, https://laravel.com/docs/10.x/eloquent

- Diagrama de Clases UML, https://www.youtube.com/watch?v=Z0yLerU0g-Q

- Diagrama de Clases con Star UML, https://www.youtube.com/watch?v=Wp3xW1WgQ8I

##	Anexos
### Foto de la Empresa
![diagrama](https://live.staticflickr.com/65535/53564573887_7b9e0ea1a1_z.jpg)

### Datos de Ubicación
https://www.google.com/maps/dir//Av.+El+Trompillo+300,+Santa+Cruz+de+la+Sierra/@-17.8066407,-63.2666183,12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x93f1e8162bd3345d:0xbfcf5ab6ce52c6c1!2m2!1d-63.1908182!2d-17.8004235?hl=es&entry=ttu