# proyecto formativo: Modelado Backend de un sistema para la administracion de un taller de motos.

-   FAVIO AVENDAÑO ADRIAN
-   [Facebook](https://www.facebook.com/fabian.avendano.7921?mibextid=ZbWKwL)

## Introducción

este sistema esta creado para poder administrar un taller de motos que tiene problemas a la hora de registrar deudores ,personas que recogen la motocicletas luego de haber pasado bastante tiempo tambien ocurre problemas al momento de cobrar no se sabe quien fue el que reparo dicha motocicleta tampoco se sabe que reparaciones le hicieron y en que estado estaba la moto

## Objetivos

Desarrollar el Analisis, Modelado, Migraciones, Models, Seeder y la Api REst Para lograr que todos los datos queden almacenados y no se pierdan como lo esta pasando actualmente el taller de motos Manantial Del distrito 2 Chane independencia

## Marco Teórico

### Laravel

Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

```bash
composer install
```

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project laravel/laravel ApiRest
```

3. **Crear un nuevo modelo**: Utiliza php artizan para crear un modelo llamado "Customer" El flag --all crea también las migraciones, el factory y el seeder asociados al modelo.

```bash
php artisan make:model Customer --all
```

4. **Ejecutar Migraciones**: Una vez que has creado tu modelo y definido la estructura de tu base de datos, necesitas ejecutar las migraciones para crear las tablas correspondientes en la base de datos. Utiliza el siguiente comando:

```bash
php artisan migrate
```

5. **Sembrar la Base de Datos (Opcional)**: Si deseas poblar tu base de datos con datos de prueba, puedes utilizar seeders. Esto es útil para tener datos de muestra con los que trabajar durante el desarrollo. Ejecuta el siguiente comando para ejecutar los seeders:

```bash
php artisan db:seed
```

6. **Crear un Recurso de Colección**: Para manejar la transformación de una colección de modelos de clientes antes de devolverlos como respuestas JSON, vamos a crear un recurso de colección llamado "CustomerCollection". Esto nos permitirá estructurar y personalizar la salida de la API para las colecciones de recursos. Ejecuta el siguiente comando:

```bash
php artisan make:resource CustomerCollection
```

7. **Acceder a la API Local**: Una vez que tu proyecto está en funcionamiento, puedes acceder a la API localmente utilizando la dirección proporcionada. Si estás siguiendo las convenciones típicas de Laravel, la dirección de tu API sería algo así como http://127.0.0.1:8000/api/v1/customers. Asegúrate de reemplazar "8000" con el puerto en el que está ejecutando tu servidor local, si es diferente.

```bash
php artisan serve
```

### Backend

El backend se refiere a la parte de un sistema informático que maneja la lógica y la funcionalidad interna, así como la interacción con la base de datos. Es la parte del sistema que no es visible para los usuarios finales, ya que se encarga de procesar la información, gestionar la lógica de negocio y trabajar en conjunto con el frontend, que es la interfaz de usuario visible.

### MVC

es un patrón de diseño arquitectónico utilizado comúnmente en el desarrollo de software, especialmente en aplicaciones web. Este patrón separa la aplicación en tres componentes principales:

#### Modelo (Model): Representa los datos y la lógica de negocio de la aplicación. El modelo gestiona la manipulación y el acceso a los datos, así como las reglas de negocio asociadas.

#### Vista (View): Es responsable de mostrar la información al usuario y de presentar la interfaz de usuario. La vista obtiene los datos del modelo y los muestra de una manera que sea comprensible para el usuario. En el contexto web, la vista a menudo se asocia con la interfaz de usuario y la presentación de las páginas web.

#### Controlador (Controller): Actúa como intermediario entre el modelo y la vista. Recibe las solicitudes del usuario, procesa la entrada y coordina las acciones del modelo y la vista en consecuencia. El controlador maneja la lógica de la aplicación y actualiza el modelo y la vista según sea necesario.

### postman

es una herramienta que facilita el desarrollo y prueba de APIs (Interfaz de Programación de Aplicaciones). Permite enviar solicitudes HTTP a una API y recibir las respuestas, lo que ayuda a los desarrolladores a probar, depurar y documentar APIs de manera eficiente. También proporciona funciones para automatizar pruebas y colaborar en el desarrollo de APIs.

### visual studio code

es un entorno de desarrollo de código fuente gratuito y de código abierto desarrollado por Microsoft. Es una herramienta versátil que se utiliza para escribir, editar y depurar código en diversos lenguajes de programación.

## Metodología

hize un analisis al taller de motos yeriklobo y resulta que cuando llega un cliente al taller a dejar su moto este simplemente responde que lo revisara luego de desocuparse sin la necesidad de registrar al cliente o la moto o la persona que se encargara de chequear la moto.

un problema que tienen los ayudantes del taller es que cobran demaciado alto cuando no esta el dueño y esto molesta a los clientes porque el dueño les cobra mas barato entonces tenemos que mantener el precio estable

entonces el sistema que realizaremos tendra que registrar ,recuperar informacion y generar informes.
tenemos que capacitar al personal del taller sobre el nuevo sistema.
tenemos que facilitar el proceso de cambio de metodos antiguos hacia el nuevo sistema.
tenemos que monitoriar el sistema para que funcieno de forma segura.
obtener encuesta de los clientes y dueño para realizar mejoras.
implementar un seguimiento de pago pendiente y el estado actual de las motos.
Implementar rutinas regulares de respaldo de datos para evitar la pérdida de información valiosa.
Establecer protocolos de recuperación en caso de fallas o pérdida de datos.

seguir estos pasos ayudara a que no tenga errores

```bash
composer create-project laravel/laravel ApiRest
php artisan make:model Customer –all
php artisan key:generate
##orden de migracion por el tema de claves foráneas no se puede migrar todo automatico
php artisan migrate --path=database/migrations/2024_03_01_214903_create_clientes_table.php
php artisan migrate --path=database/migrations/2024_03_01_215003_create_motos_table.php
php artisan migrate --path=database/migrations/2024_03_01_215024_create_empleados_table.php
php artisan migrate --path=database/migrations/2024_03_01_215008_create_servicios_table.php
php artisan migrate --path=database/migrations/2024_03_01_215013_create_repuestos_table.php
php artisan migrate --path=database/migrations/2024_03_01_215019_create_servicio_repuestos_table.php
php artisan migrate --path=database/migrations/2024_03_01_215029_create_facturas_table.php


luego subimos los seeders
php artisan db:seed
php artisan db:seed --class=ClienteSeeder
php artisan db:seed --class=MotoSeeder
php artisan db:seed --class=EmpleadoSeeder
php artisan db:seed --class=ServicioSeeder
php artisan db:seed --class=RepuestoSeeder
php artisan db:seed --class=ServicioRepuestoSeeder
php artisan db:seed --class=FacturaSeeder

php artisan make:resource ClienteCollection
php artisan make:resource ClienteResource
```

## Modelado o Sistematización

### Diagrama de clases

![diagrama](https://live.staticflickr.com/65535/53568825286_f1b492125e_z.jpg[/img][/url][url=https://flic.kr/p/2pBGerY]Captura)

### Diagrama entidad relacion.

![diagrama2](https://live.staticflickr.com/65535/53568827111_63ffc5bfa1_h.jpg[/img][/url][url=https://flic.kr/p/2pBGeZr]Captura)

### Migracion

![migracion](https://live.staticflickr.com/65535/53568829566_586caea120_b.jpg[/img][/url][url=https://flic.kr/p/2pBGfHL]Captura)
![seeders](https://live.staticflickr.com/65535/53568829921_d627f0bbc3_h.jpg[/img][/url][url=https://flic.kr/p/2pBGfPT]Captura)

### explicacion del un caso de modelado.

primero creamos las tablas Cliente,motos,servicios,repuestos,serviciorepuestos,empleados,facturas.
luego de eso pasamos a los factories que son campos que generan datos automaticamente con el faker.
de la misma forma pasamos alos seeders y usamos la fabrica de los factories para insertar cualquier cantidad del modelo de datos a la base de datos.
teniendo todo esto ya deberiamos poder ver nuestra base de datos con datos.
lo siguiente es crear models de cada uno de estos y colocar las relaciones entre ellos y especificamos los campos que seran llenados de forma masiva.
luego programamos los controladores referente a lo que necesitaremos.
pasamos a los reques para lograr usar los controladores como necesitamos.
en mi caso programamos filtros para poder filtrar apis de forma mas intuitiva y controlada y poder actualizarla en cualquier momento.
en las apis creamos un grupo de apis para contralar nuestras apis versionadas por el momento estan en la version 1 y para no complicarnos la vida solo llamamos al controlador

### php artisan tinker

```bash
C:\laragon\www\taller-de-moto
λ php artisan tinker
Psy Shell v0.12.0 (PHP 8.1.10 — cli) by Justin Hileman
> $clientes = App\Models\Cliente::all();
= Illuminate\Database\Eloquent\Collection {#6067
    all: [
      App\Models\Cliente {#6068
        id: 1,
        nombre: "Janelle",
        apellido: "Bauch",
        telefono: "+1.360.210.5502",
        direccion: """
          846 Colin Plaza\n
          Bethelberg, WA 13709-1783
          """,
        correoElectronico: "kcrooks@example.net",
        created_at: "2024-03-05 07:38:27",
        updated_at: "2024-03-05 07:38:27",
        deleted_at: null,
      },
      App\Models\Cliente {#6069
        id: 2,
        nombre: "Dayton",
        apellido: "Kozey",
        telefono: "+15866584177",
        direccion: """
          143 Eldon Squares Suite 457\n
          Eusebioview, AL 74948-3849
          """,
        correoElectronico: "lizeth10@example.net",
        created_at: "2024-03-05 07:38:27",
        updated_at: "2024-03-05 07:38:27",
        deleted_at: null,
      },

> $cliente = App\Models\Cliente::find(1);
= App\Models\Cliente {#6019
    id: 1,
    nombre: "Janelle",
    apellido: "Bauch",
    telefono: "+1.360.210.5502",
    direccion: """
      846 Colin Plaza\n
      Bethelberg, WA 13709-1783
      """,
    correoElectronico: "kcrooks@example.net",
    created_at: "2024-03-05 07:38:27",
    updated_at: "2024-03-05 07:38:27",
    deleted_at: null,
  }

> $cliente->telefono = '987654321';
= "987654321"

> $cliente->save();
= true

> $cliente = App\Models\Cliente::find(1);
= App\Models\Cliente {#6021
    id: 1,
    nombre: "Janelle",
    apellido: "Bauch",
    telefono: "987654321",
    direccion: """
      846 Colin Plaza\n
      Bethelberg, WA 13709-1783
      """,
    correoElectronico: "kcrooks@example.net",
    created_at: "2024-03-05 07:38:27",
    updated_at: "2024-03-05 10:01:01",
    deleted_at: null,
  }

> $cliente->delete();
= true
```

### Datos seeder

Aquí estamos utilizando una factory para crear datos clientes.
Luego, se repite este proceso

```bash
<?php

// En database/seeders/ClienteSeeder.php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cliente;

class ClienteSeeder extends Seeder
{
    public function run()
    {
        // Usamos la fábrica ClienteFactory para crear 10 instancias del modelo Cliente y las insertamos en la base de datos
        Cliente::factory()->count(10)->create();
    }
}

```

### APi.

-   http://127.0.0.1:8000/api/v1/clientes
-   http://127.0.0.1:8000/api/v1/motos
-   http://127.0.0.1:8000/api/v1/empleados
-   http://127.0.0.1:8000/api/v1/facturas
-   http://127.0.0.1:8000/api/v1/motos
-   http://127.0.0.1:8000/api/v1/servicios
-   http://127.0.0.1:8000/api/v1/serviciosrepuestos
-   http://127.0.0.1:8000/api/v1/servicios?fecha[gt]=2018-04-03
-   http://127.0.0.1:8000/api/v1/motos?marca[eq]=ducati
-   http://127.0.0.1:8000/api/v1/clientes/5
-

## Conclusiones

en conclucion no llegamos hasta donde queremos llegar pero el proyecto ya tiene casi todo terminado en parte del cliente y del servidor. faltarian las demas

## Bibliografía

-   Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia, https://www.youtube.com/watch?v=abcd1234
-   https://chat.openai.com/
-   https://youtu.be/ZPsjGNPGV8c?si=OlwgDgIGDaEEG8vL

## Anexos

![tallermoto](https://live.staticflickr.com/65535/53567993702_8f3efbff0e_b.jpg[/img][/url][url=https://flic.kr/p/2pBBYfj]WhatsApp)
![tallermoto](https://live.staticflickr.com/65535/53569286185_66d525b524_b.jpg[/img][/url][url=https://flic.kr/p/2pBJAsv]WhatsApp)

### ubicacion

https://maps.app.goo.gl/JmTeeD2Z6ANEHsys8
